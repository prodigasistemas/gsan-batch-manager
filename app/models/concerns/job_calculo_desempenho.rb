class JobCalculoDesempenho

  attr_reader :processo, :ano_mes_referencia

  PARAMETROS = {
    "anoMesFaturamento" => :ano_mes_referencia,
  }

  def initialize processo, params
    @processo = processo
    @ano_mes_referencia = params[:ano_mes_referencia]
  end

  def inicia_processo
    novo_processo_iniciado = processo.processos_iniciados.build processo_iniciado_params
    novo_processo_iniciado.save!

    if @ano_mes_referencia.present?
      PARAMETROS.keys.each do |parametro|
        novo_processo_iniciado.parametros << ProcessoParametro.new(nome: parametro, valor: self.send(PARAMETROS[parametro]))
      end
      novo_processo_iniciado.save!
    end

    @ano_mes_referencia = "#{Time.zone.now.year}#{Time.zone.now.month.to_s.rjust(2, '0')}" if @ano_mes_referencia.nil?

    novo_processo_iniciado.iniciar_atividades ContratoMedicao.por_referencia(@ano_mes_referencia)

    novo_processo_iniciado
  end

  def processo_iniciado_params
    processo_iniciado = {
      ultima_alteracao: Time.now,
      usuario: Usuario.first, # Recuperar o Usuário
      situacao: ProcessoSituacao.find(ProcessoSituacao::SITUACAO[:em_espera]),
      prioridade: @processo.prioridade
    }

    processo_iniciado
  end
end