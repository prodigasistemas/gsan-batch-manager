class ContratoMedicao < ActiveRecord::Base
  self.table_name='desempenho.contrato_medicao'
  self.primary_key='cmed_id'

  def self.por_referencia(referencia)
    data_referencia_inicial = Date.new(referencia[0,4].to_i, referencia[4,5].to_i, 1)
    data_referencia_final = Date.new(referencia[0,4].to_i, referencia[4,5].to_i, -1).end_of_month

    where('cmed_vigencia_inicial <= ? and cmed_vigencia_final >= ?', data_referencia_inicial, data_referencia_final)
  end
end