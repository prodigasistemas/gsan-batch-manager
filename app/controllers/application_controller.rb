require 'digest/md5'

class ApplicationController < ActionController::Base
  include LinksHelper
  
  protect_from_forgery with: :exception

  before_action :acesso_restrito, :carrega_situacoes

  def carrega_situacoes
    @situacoes = ProcessoSituacao.all.map(&:descricao)
  end

 protected

  def usuario_logado
    if token = cookies[:gsan]
      id, expiracao = verifier.verify(token)

      if Time.zone.now < expiracao
        Usuario.find(id)
      end
    else
      nil
    end
  end

  def acesso_restrito
    if cookies[:gsan]
      @usuario_logado = usuario_logado
      return @usuario_logado unless @usuario_logado.nil?
    end
    session.clear

    redirect_to(login_url)
  end

  private

  def verifier
    @verifier ||= ActiveSupport::MessageVerifier.new('gsan', serializer: YAML)
  end

end
